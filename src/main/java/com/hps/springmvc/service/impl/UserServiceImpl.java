package com.hps.springmvc.service.impl;

import com.hps.springmvc.entity.User;
import com.hps.springmvc.mapper.UserMapper;
import com.hps.springmvc.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author heps
 * @date 2019/1/22 10:00
 */
@Service
public class UserServiceImpl implements UserService {

    private UserMapper userMapper;

    public UserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public User findById(Long id) {
        return userMapper.selectByPrimaryKey(id);
    }
}

package com.hps.springmvc.service;

import com.hps.springmvc.entity.User;

/**
 * @author heps
 * @date 2019/1/22 10:00
 */
public interface UserService {

    User findById(Long id);
}

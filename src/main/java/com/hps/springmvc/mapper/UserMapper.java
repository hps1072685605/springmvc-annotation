package com.hps.springmvc.mapper;

import com.hps.springmvc.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author heps
 * @date 2019/1/22 9:54
 */
@Repository
public interface UserMapper {

    User selectByPrimaryKey(Long id);
}

package com.hps.springmvc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 读取Mybatis配置信息
 * @author heps
 * @date 2019/1/22 9:17
 */
@Configuration
@PropertySource("classpath:/mybatis.properties")
public class MybatisPropertiesConfig {
}
